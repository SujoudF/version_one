from core.sortHotels import sortHotels
#from core.writeData import writeData
import csv
from pages.searchResultsPage import searchResultsPage
from pages.yamsaferSearch import yamsaferSearch
from core.readProperties import readProperties
from core.openCsv import openCsv
from pages.readSearchResults import readSearchResults
from ddt import *
import xmlrunner
import unittest
from selenium import webdriver
def readData():
    openCsvInc=openCsv()
    arr = openCsvInc.openCSV("../sources/yamsaferData.csv")
    return arr
@ddt
class mainTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.browserType = readProperties.readp('../sources/browserCheck.properties', 'browsertype')
        if (cls.browserType == "Firefox"):
            firefox = "../drivers/geckodriver.exe"
            cls.browser = webdriver.Firefox(executable_path=firefox)
        elif (cls.browserType == "Chrome"):
            cls.browser = webdriver.Chrome(executable_path='../drivers/chromedriver.exe')
        cls.browser.maximize_window()
        try:
            cls.write_file = open('../sources/SortedHotelsFile.csv', 'w', newline='')
            cls.write_file.write('[Name,URL,Rate,Price]' + '\n')
        except IOError as e:
            print("I/O error({0}): {1}".format(e.errno, e.strerror))

    @data(*readData())
    @unpack
    def testMethod(self,City,checkindate,checkoutdate,noofrooms,noofadults,noofchildren,childrenage,star):
        yamsaferSearchIns=yamsaferSearch()
        yamsaferSearchIns.yamsaferSearchMethod(self.browser,City)
        yamsaferSearchIns.numOfPeople(self.browser,noofrooms,noofadults,noofchildren,childrenage)
        yamsaferSearchIns.dateMethod(self.browser,checkindate,checkoutdate)
        yamsaferSearchIns.searchButton(self.browser)
        searchResultsPageIns=searchResultsPage()
        searchResultsPageIns.hotelStar(self.browser,star)
        searchResultsPageIns.bestReviewedHotel(self.browser)
        readSearchResultsIns=readSearchResults()
        hotelsList=readSearchResultsIns.resultsMethod(self.browser)
        sortHotelsIns=sortHotels()
        sortedList=sortHotelsIns.sortHotelsMethod(hotelsList)

        self.write_file.writelines(str(sortedList) + '\n')
        #r = csv.writer(self.file)
        #r.writerow(list)

    @classmethod
    def tearDownClass(cls):
        #cls.browser.quit()
        cls.write_file.close()


if __name__ == '__main__':
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='YamsaferReport'), failfast=False, catchbreak=False)