from sources import locator
class readSearchResults:
    def resultsMethod(self,browser):
        rateslist= []
        nameslist = []
        priceslist=[]
        linkslist=[]
        names=browser.find_elements_by_xpath(locator.LIST_OF_HOTEL_NAMES)
        for l in names:
            nameslist.append(l.text)
        rates=browser.find_elements_by_xpath(locator.LIST_OF_HOTEL_RATE)
        for l in rates:
            rateslist.append(l.text)
        prices=browser.find_elements_by_xpath(locator.LIST_OF_HOTEL_PRICES)
        for l in prices:
            pricesint=(l.text).split()
            pricesintnum=str(pricesint[1]).replace(',','')
            priceslist.append(int(pricesintnum))
        links=browser.find_elements_by_xpath(locator.LIST_OF_LINKS)
        for l in links:
            linkslist.append(l.get_attribute("href"))
        listoflists=[nameslist,linkslist,rateslist,priceslist]
        return listoflists