import time
from sources import locator
class searchResultsPage:
    def hotelStar(self, browser, star):
        if star == str(5):
            browser.find_element_by_xpath(locator.FIVE_STARS).click()
        elif star == str(4):
            browser.find_element_by_xpath(locator.FOUR_STARS).click()
        elif star == str(3):
            browser.find_element_by_xpath(locator.THREE_STARS).click()
        elif star == str(2):
            browser.find_element_by_xpath(locator.TWO_STARS).click()
        elif star == str(1):
            browser.find_element_by_xpath(locator.ONE_STAR).click()
        time.sleep(5)
    def bestReviewedHotel(self, browser):
        browser.find_element_by_xpath(locator.BEST_REVIEWED).click()
        time.sleep(5)