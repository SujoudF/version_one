from setuptools import setup
setup(
    name="Flow Test",
    description=("An demonstration of how to create, document, and publish "
                 "to the cheese shop a5 pypi.org."),
    license="BSD",
    keywords="example documentation tutorial",
    packages=['core', 'drivers', 'pages', 'sources', 'tests'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: BSD License",
    ],
)