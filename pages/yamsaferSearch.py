from selenium.webdriver.common.keys import Keys
from selenium.webdriver import ActionChains
from core.readProperties import readProperties
from sources import locator
import time
class yamsaferSearch :
    def __init__(self):
        "constracture"
    def yamsaferSearchMethod(self,browser,city):#search on specific city
        url = readProperties.readp('../sources/browserCheck.properties', 'siteUrl')
        browser.get(url)
        time.sleep(5)
        citySearchArea=browser.find_element_by_xpath(locator.SEARCH_CITY)
        citySearchArea.click()
        citySearchArea.send_keys(Keys.CONTROL + "a")#delete previous data
        citySearchArea.send_keys(Keys.DELETE)
        time.sleep(5)
        citySearchArea.send_keys(city)#write city
        time.sleep(5)
        browser.find_element_by_xpath(locator.CITY).click()
        time.sleep(10)
    def dateMethod(self,browser,checkinDate,checkOutDate):
         checkinDateArr=checkinDate.split('_')#split day from month and year
         checkOutDateArr=checkOutDate.split('_')
         checkInMonth = str(checkinDateArr[1]+' '+checkinDateArr[2])
         checkOutMonth=str(checkOutDateArr[1]+' '+checkOutDateArr[2])
         dateButton = browser.find_element_by_xpath(locator.DATE_BUTTON)
         dateButton.click()
         currentMonthYear=browser.find_element_by_xpath(locator.CURRENT_MONTH).text
         while currentMonthYear != checkInMonth:#get the needed month
             browser.find_element_by_xpath(locator.NEXT_MONTH_BUTTON).click()
             currentMonthYear = browser.find_element_by_xpath(locator.CURRENT_MONTH).text
         browser.find_element_by_xpath(locator.PRE_MONTH_BUTTON).click()
         time.sleep(5)
         yamsaferSearchIns=yamsaferSearch()#pick the needed day
         chin=yamsaferSearchIns.seelectXpathDate(checkInMonth,checkinDateArr[0])
         chout=yamsaferSearchIns.seelectXpathDate(checkOutMonth,checkOutDateArr[0])
         browser.find_element_by_xpath(chin).click()
         browser.find_element_by_xpath(chout).click()
         time.sleep(5)
    def numOfPeople(self,browser,noofrooms,noofadults,noofchildren,childrenage):
        age =childrenage.split()
        browser.find_element_by_xpath(locator.ADULT_CHILD_BUTTON).click()
        currentroom = browser.find_element_by_xpath(locator.ROOM_NUM_TEXT).text
        currentadults = browser.find_element_by_xpath(locator.ADULT_NUM_TEXT).text
        currentchildren = browser.find_element_by_xpath(locator.CHILDREN_NUM_TEXT).text
        while currentroom != '1 Room':#release data
            browser.find_element_by_xpath(locator.DEC_ROOM_NUM).click()
            currentroom = browser.find_element_by_xpath(locator.ROOM_NUM_TEXT).text
        while currentadults != '1 Adult':
            browser.find_element_by_xpath(locator.DEC_ADULT_NUM).click()
            currentadults = browser.find_element_by_xpath(locator.ADULT_NUM_TEXT).text
        while currentchildren != 'No Children':
            browser.find_element_by_xpath(locator.DEC_CHILDREN_NUM).click()
            currentchildren = browser.find_element_by_xpath(locator.CHILDREN_NUM_TEXT).text
        time.sleep(5)
        while currentroom != noofrooms:#change number of rooms
            browser.find_element_by_xpath(locator.INC_ROOM_NUM).click()
            currentroom = browser.find_element_by_xpath(locator.ROOM_NUM_TEXT).text
        if noofadults > currentadults:#change num of adults
            while currentadults != noofadults:
                browser.find_element_by_xpath(locator.INC_ADULT_NUM).click()
                currentadults = browser.find_element_by_xpath(locator.ADULT_NUM_TEXT).text
        if noofchildren != 'No Children':#change num of children
            while currentchildren != noofchildren:
                browser.find_element_by_xpath(locator.INC_CHILDREN_NUM).click()
                currentchildren = browser.find_element_by_xpath(locator.CHILDREN_NUM_TEXT).text
            if noofchildren != 0:#select children age
                slider = browser.find_elements_by_xpath(locator.CHILDREN_AGE)
                n=0
                for l in slider:
                    move = ActionChains(browser)
                    move.click_and_hold(l).move_by_offset((float(age[n])*10), 0).release().perform()
                    n=n+1
        time.sleep(3)

    def searchButton(self,browser):
        browser.find_element_by_xpath(locator.SEARCH_BUTTON).click()
        time.sleep(5)
    def seelectXpathDate(self,monthYear,day):#build dynaminc path for date
        xp1=str('//table[@name="')
        xp2=str('"]/tbody/tr/td/div/div[text()=')
        xp3=str(']')
        xp=xp1+monthYear+xp2+day+xp3
        return xp