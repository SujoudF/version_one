from core.sortHotels import sortHotels
from pages.searchResultsPage import searchResultsPage
from pages.yamsaferSearch import yamsaferSearch
from core.readProperties import readProperties
from core.openCsv import openCsv
from pages.readSearchResults import readSearchResults
from ddt import *
import xmlrunner
import unittest
from selenium import webdriver
def readData():#open csv file
    openCsvInc=openCsv()
    arr = openCsvInc.openCSV("../sources/oneTestCase.csv")
    return arr
@ddt
class mainTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):#open browser and file to write on
        cls.browserType = readProperties.readp('../sources/browserCheck.properties', 'browsertype')
        if (cls.browserType == "Firefox"):
            firefox = "../drivers/geckodriver.exe"
            cls.browser = webdriver.Firefox(executable_path=firefox)
        elif (cls.browserType == "Chrome"):
            cls.browser = webdriver.Chrome(executable_path='../drivers/chromedriver.exe')
        cls.browser.maximize_window()
        try:
            cls.write_file = open('../sources/SortedHotelsFile.csv', 'w', newline='')
            cls.write_file.write('[Name,URL,Rate,Price]' + '\n')
        except IOError as e:
            print("I/O error({0}): {1}".format(e.errno, e.strerror))

    @data(*readData())
    @unpack
    def testMethod(self,City,checkindate,checkoutdate,noofrooms,noofadults,noofchildren,childrenage,star):
        yamsaferSearchIns=yamsaferSearch()
        yamsaferSearchIns.yamsaferSearchMethod(self.browser,City)#print city name
        yamsaferSearchIns.dateMethod(self.browser, checkindate, checkoutdate)#select date
        yamsaferSearchIns.numOfPeople(self.browser,noofrooms,noofadults,noofchildren,childrenage)#select room,adulrs and children numbers
        yamsaferSearchIns.searchButton(self.browser)#press search button
        searchResultsPageIns=searchResultsPage()
        searchResultsPageIns.hotelStar(self.browser,star)#select the hotel star
        searchResultsPageIns.bestReviewedHotel(self.browser)#select best reviewed sort
        readSearchResultsIns=readSearchResults()#read results
        hotelsList=readSearchResultsIns.resultsMethod(self.browser)
        sortHotelsIns=sortHotels()#sort hotels
        sortedList=sortHotelsIns.sortHotelsMethod(hotelsList)
        for list in sortedList:
            for inlist in list:
                self.write_file.writelines(str(inlist) + '\n')  # write on csv





    @classmethod
    def tearDownClass(cls):

        cls.write_file.close()


if __name__ == '__main__':
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output='YamsaferReport'), failfast=False, catchbreak=False)