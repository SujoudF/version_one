class sortHotels:
    def sortHotelsMethod(self,hotelsList):
        rateArrayLength=len(hotelsList[2])
        veryBad=[]
        bad=[]
        good=[]
        veryGood=[]
        excellent=[]
        i=0
        while i <= rateArrayLength-1:#classify data to sort according to price
            hotelrate=hotelsList[2][i].split('/')
            if float(hotelrate[0]) >= 4.0:
                excellent.append([hotelsList[0][i],hotelsList[1][i],hotelsList[2][i],hotelsList[3][i]])
            elif float(hotelrate[0]) >= 3.0 and  float(hotelrate[0]) < 4.0:
                veryGood.append([hotelsList[0][i],hotelsList[1][i],hotelsList[2][i],hotelsList[3][i]])
            elif float(hotelrate[0]) >= 2.0 and  float(hotelrate[0]) < 3.0:
                good.append([hotelsList[0][i],hotelsList[1][i],hotelsList[2][i],hotelsList[3][i]])
            elif float(hotelrate[0]) >= 1.0 and float(hotelrate[0]) < 2.0:
                bad.append([hotelsList[0][i],hotelsList[1][i],hotelsList[2][i],hotelsList[3][i]])
            elif float(hotelrate[0]) >= 0.0 and float(hotelrate[0]) < 1.0:
                veryBad.append([hotelsList[0][i],hotelsList[1][i],hotelsList[2][i],hotelsList[3][i]])
            i=i+1
            #sort according to price
        excellent.sort(key=lambda x: x[3])
        veryGood.sort(key=lambda x: x[3])
        good.sort(key=lambda x: x[3])
        bad.sort(key=lambda x: x[3])
        veryBad.sort(key=lambda x: x[3])
        sortedArray=[excellent,veryGood,good,bad,veryBad]
        return sortedArray