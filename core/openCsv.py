import csv
class openCsv:
    def openCSV(self,filename):#open file and read data
        readArray = []
        with open(filename) as csv_file:
            csv_reader = csv.reader(csv_file)
            next(csv_reader)
            for row in csv_reader:
                readArray[len(readArray):] = [row]
            return readArray
